FROM golang:1.21.1 AS builder
WORKDIR /app
COPY go.mod .
COPY go.sum .
RUN go mod download
COPY . .
COPY .env .

RUN CGO_ENABLED=0 GOOS=linux go build -o app ./apps/api

FROM alpine:latest
WORKDIR /app
COPY --from=builder /app/app .
COPY --from=builder /app/.env .
COPY --from=builder /app/migration /app/migration


EXPOSE 9191
CMD ["./app"]
