package config

import (
	"github.com/vandyahmad24/alat-bantu/config"
	"log"
)

var (
	configPath = "./.env"
	EnvConfig  = NewConfig()
)

type Config struct {
	PORT              string
	POSTGRES_USERNAME string
	POSTGRES_PASSWORD string
	POSTGRES_HOST     string
	POSTGRES_PORT     string
	POSTGRES_DATABASE string
	MIGRATION_EVENT   string
	OPENAPI_TOKEN     string
	REDIS_HOST        string
	REDIS_PORT        string
	REDIS_PASSWORD    string
}

func NewConfig() *Config {
	if err := config.LoadEnv(configPath); err != nil {
		log.Fatalf("Application dimissed. Application cannot find %s to run this application", err.Error())
	}

	return &Config{
		PORT:              config.GetEnv("PORT", ""),
		POSTGRES_USERNAME: config.GetEnv("POSTGRES_USERNAME", ""),
		POSTGRES_PASSWORD: config.GetEnv("POSTGRES_PASSWORD", ""),
		POSTGRES_HOST:     config.GetEnv("POSTGRES_HOST", ""),
		POSTGRES_PORT:     config.GetEnv("POSTGRES_PORT", ""),
		POSTGRES_DATABASE: config.GetEnv("POSTGRES_DATABASE", ""),
		MIGRATION_EVENT:   config.GetEnv("MIGRATION_EVENT", ""),
		OPENAPI_TOKEN:     config.GetEnv("OPENAPI_TOKEN", ""),
		REDIS_HOST:        config.GetEnv("REDIS_HOST", ""),
		REDIS_PORT:        config.GetEnv("REDIS_PORT", ""),
		REDIS_PASSWORD:    config.GetEnv("REDIS_PASSWORD", ""),
	}

}
