package config

import (
	"context"
	"fmt"
	logger2 "gorm.io/gorm/logger"
	"vandyahmad/avatara/lib"
	"vandyahmad/avatara/lib/logger"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func NewPG() (*lib.Database, error) {
	username := EnvConfig.POSTGRES_USERNAME
	password := EnvConfig.POSTGRES_PASSWORD
	host := EnvConfig.POSTGRES_HOST
	port := EnvConfig.POSTGRES_PORT
	dbname := EnvConfig.POSTGRES_DATABASE

	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Jakarta", host, username, password, dbname, port)
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		Logger: logger2.Default.LogMode(logger2.Silent),
	})
	if err != nil {
		return nil, err
	}

	logger.Info(context.Background(), "successfully connected to postgres", make(map[string]interface{}))
	return &lib.Database{DB: db}, nil
}
