package usecase

import (
	"context"
	"errors"
	. "github.com/smartystreets/goconvey/convey"
	"github.com/stretchr/testify/mock"
	"testing"
	"vandyahmad/avatara/request"
)

func TestReadChat(t *testing.T) {
	ctx := context.Background()

	Convey("Read Chat Usecase Scenario", t, func() {

		Convey("fail Get Chat", func() {
			r1 := m.On("GetChatCount", mock.Anything).Return(1, nil)
			r2 := mRedis.On("Get", "chat-message").Return("", errors.New("error"))
			res, err := u.ReadChat(ctx)
			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)
			r1.Unset()
			r2.Unset()
		})

		Convey("fail Unmarsal", func() {
			r1 := m.On("GetChatCount", mock.Anything).Return(1, nil)
			r2 := mRedis.On("Get", "chat-message").Return("", nil)
			res, err := u.ReadChat(ctx)
			So(res, ShouldBeNil)
			So(err, ShouldNotBeNil)
			r1.Unset()
			r2.Unset()
		})

		Convey("Sukses Get Chat", func() {
			r1 := m.On("GetChatCount", mock.Anything).Return(1, nil)
			r2 := mRedis.On("Get", "chat-message").Return(`[{"from":"user","body":"Apa perbedaan dari singa dan harimau","created_at":"2024-01-09T22:21:58.020419805+07:00"}]`, nil)
			res, err := u.ReadChat(ctx)
			So(res, ShouldNotBeNil)
			So(err, ShouldBeNil)
			r1.Unset()
			r2.Unset()
		})
		//

	})
}

func TestCreateChat(t *testing.T) {
	ctx := context.Background()

	Convey("Create Chat Usecase Scenario", t, func() {

		Convey("Failed GetChatCount", func() {
			r1 := m.On("GetChatCount", mock.Anything).Return(0, errors.New("errors"))
			res, err := u.CreateChat(ctx, request.CreateChat{})
			So(res, ShouldNotBeNil)
			So(err, ShouldNotBeNil)
			r1.Unset()

		})

		Convey("IF Chat 0 Failed SendNewMessage", func() {
			r1 := m.On("GetChatCount", mock.Anything).Return(0, nil)
			r2 := mChat.On("SendNewMessage", mock.Anything).Return("", errors.New("erros"))
			res, err := u.CreateChat(ctx, request.CreateChat{})
			So(res, ShouldNotBeNil)
			So(err, ShouldNotBeNil)
			r1.Unset()
			r2.Unset()

		})

	})
}
