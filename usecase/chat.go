package usecase

import (
	"context"
	"encoding/json"
	"time"
	"vandyahmad/avatara/lib/logger"
	"vandyahmad/avatara/model"
	"vandyahmad/avatara/request"
)

func (usecase *Usecase) CreateChat(ctx context.Context, payload request.CreateChat) (model.Chat, error) {
	var chat model.Chat
	logger.Info(ctx, "Payload CreateChat", map[string]interface{}{
		"payload": payload,
		"tags":    []string{"postgres", "chat", "usecase"},
	})

	//get chat first
	countChat, err := usecase.repo.GetChatCount(ctx)
	if err != nil {
		logger.Error(ctx, "Error CreateChat", map[string]interface{}{
			"payload": payload,
			"tags":    []string{"postgres", "chat", "usecase"},
		})
		return chat, err
	}

	//jika chat baru pertama maka create
	if countChat == 0 {

		response, err := usecase.repoChat.SendNewMessage(ctx, payload.Message)
		if err != nil {
			logger.Error(ctx, "Error CreateChat", map[string]interface{}{
				"error": err,
				"tags":  []string{"postgres", "chat", "usecase"},
			})
			return chat, err
		}

		msgData := []model.MessageData{
			{
				From:      "user",
				Body:      payload.Message,
				CreatedAt: time.Now(),
			},
			{
				From:      "chat-gpt",
				Body:      response,
				CreatedAt: time.Now(),
			},
		}

		chat.ChatData = msgData

		chat, err = usecase.repo.CreateChat(ctx, chat)
		if err != nil {
			logger.Error(ctx, "Error CreateChat", map[string]interface{}{
				"error": err,
				"tags":  []string{"postgres", "chat", "usecase"},
			})
			return chat, err
		}

		// add to redis
		err = usecase.repoRedis.Set("chat-message", string(chat.Message), 0)

	} else {
		// jika sudah ada maka update
		//get chat
		chat, _ = usecase.repo.GetLastChat(ctx)

		var tempMsg []model.MessageData

		err := json.Unmarshal(chat.Message, &tempMsg)
		if err != nil {
			logger.Error(ctx, "Error Parsing Chat", map[string]interface{}{
				"error": err,
				"tags":  []string{"postgres", "chat", "usecase"},
			})
			return model.Chat{}, err
		}

		msgData := model.MessageData{
			From:      "user",
			Body:      payload.Message,
			CreatedAt: time.Now(),
		}

		tempMsg = append(tempMsg, msgData)

		//get response from chat-gpt
		response, err := usecase.repoChat.SendOldMessage(ctx, payload.Message, tempMsg)
		if err != nil {
			logger.Error(ctx, "Error CreateChat", map[string]interface{}{
				"error": err,
				"tags":  []string{"postgres", "chat", "usecase"},
			})
			return chat, err
		}

		msgResponse := model.MessageData{
			From:      "chat-gpt",
			Body:      response,
			CreatedAt: time.Now(),
		}

		tempMsg = append(tempMsg, msgResponse)

		chat.ChatData = tempMsg

		chat, err = usecase.repo.UpdateChat(ctx, chat)
		if err != nil {
			logger.Error(ctx, "Error CreateChat", map[string]interface{}{
				"error": err,
				"tags":  []string{"postgres", "chat", "usecase"},
			})
			return chat, err
		}

		err = usecase.repoRedis.Set("chat-message", string(chat.Message), 0)
	}

	return chat, nil
}

func (usecase *Usecase) ReadChat(ctx context.Context) ([]model.MessageData, error) {
	countChat, _ := usecase.repo.GetChatCount(ctx)
	if countChat > 0 {
		response, err := usecase.repoRedis.Get("chat-message")
		if err != nil {
			return nil, err
		}

		var tempMsg []model.MessageData

		err = json.Unmarshal([]byte(response), &tempMsg)
		if err != nil {
			logger.Error(ctx, "Error Parsing Chat", map[string]interface{}{
				"error": err,
				"tags":  []string{"postgres", "chat", "usecase"},
			})
			return nil, err
		}

		return tempMsg, nil
	}
	return nil, nil

}
