package usecase

import (
	"vandyahmad/avatara/repository/interface_repo"
	"vandyahmad/avatara/repository/interface_repo/interface_chatgpt"
	"vandyahmad/avatara/repository/interface_repo/interface_redis"
	mock_repo "vandyahmad/avatara/repository/mock"
	"vandyahmad/avatara/repository/mock/mock_chat"
	"vandyahmad/avatara/repository/mock/redis_mock"
)

var m = &mock_repo.MockRepository{}
var mChat = &mock_chat.MockChatGPTRepository{}
var mRedis = &redis_mock.MockRedisRepository{}
var u = NewUsecase(m, mChat, mRedis)

type Usecase struct {
	repo      interface_repo.BaseRepository
	repoChat  interface_chatgpt.ChatGPTRepository
	repoRedis interface_redis.RedisRepository
}

func NewUsecase(
	repo interface_repo.BaseRepository,
	repoChat interface_chatgpt.ChatGPTRepository,
	repoRedis interface_redis.RedisRepository,
) Usecase {
	return Usecase{repo: repo, repoChat: repoChat, repoRedis: repoRedis}
}
