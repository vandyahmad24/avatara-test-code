package backendskeleton

import (
	"vandyahmad/avatara/lib"
	"vandyahmad/avatara/repository"
	"vandyahmad/avatara/usecase"
)

type BackendSkeleton struct {
	Usecase *usecase.Usecase
}

func NewBackendSkeleton(db *lib.Database, redis *lib.RedisRepository, openAi *lib.ChatGPTRepository) BackendSkeleton {
	repository := repository.NewRepository(db)
	usecase := usecase.NewUsecase(&repository, openAi, redis)

	return BackendSkeleton{
		Usecase: &usecase,
	}
}
