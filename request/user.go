package request

type CreateChat struct {
	Message string `json:"message" validate:"required"`
}
