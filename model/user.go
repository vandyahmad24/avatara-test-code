package model

import "time"

type Chat struct {
	ID        uint          `json:"id" gorm:"primaryKey"`
	ChatData  []MessageData `json:"chat" gorm:"-"`
	Message   []byte        `json:"-" gorm:"message"`
	CreatedAt time.Time     `json:"created_at"`
	CreatedBy uint          `json:"created_by"`
	UpdatedAt time.Time     `json:"updated_at"`
	UpdatedBy uint          `json:"updated_by"`
}

type MessageData struct {
	From      string    `json:"from"`
	Body      string    `json:"body"`
	CreatedAt time.Time `json:"created_at"`
}
