package lib

import (
	"context"
	"fmt"
	"github.com/sashabaranov/go-openai"
	"vandyahmad/avatara/model"
)

type ChatGPTRepository struct {
	client *openai.Client
}

func NewOpenAIRepostitory(key string) *ChatGPTRepository {
	client := openai.NewClient(key)
	return &ChatGPTRepository{
		client: client,
	}
}

func (r *ChatGPTRepository) SendNewMessage(ctx context.Context, message string) (string, error) {
	resp, err := r.client.CreateChatCompletion(
		ctx,
		openai.ChatCompletionRequest{
			Model: openai.GPT3Dot5Turbo,
			Messages: []openai.ChatCompletionMessage{
				{
					Role:    openai.ChatMessageRoleUser,
					Content: fmt.Sprintf("You're helpful assistant, Answer this \n %s", message),
				},
			},
		},
	)

	if err != nil {
		return "", err
	}

	return resp.Choices[0].Message.Content, nil
}

func (r *ChatGPTRepository) SendOldMessage(ctx context.Context, message string, data []model.MessageData) (string, error) {
	var conversation string
	for _, historyItem := range data {
		conversation += fmt.Sprintf("%s: %s\n", historyItem.From, historyItem.Body)
	}

	resp, err := r.client.CreateChatCompletion(
		ctx,
		openai.ChatCompletionRequest{
			Model: openai.GPT3Dot5Turbo,
			Messages: []openai.ChatCompletionMessage{
				{
					Role:    openai.ChatMessageRoleUser,
					Content: conversation,
				},
			},
		},
	)

	if err != nil {
		return "", err
	}

	return resp.Choices[0].Message.Content, nil
}
