package lib

import "golang.org/x/crypto/bcrypt"

func CustomFunction() (string, error) {
	return "", nil
}

func GenerateHashFromString(str string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(str), bcrypt.MinCost)
	if err != nil {
		return "", err
	}

	return string(hash), nil
}
