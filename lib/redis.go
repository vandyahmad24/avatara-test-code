package lib

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"log"
	"time"
)

type RedisRepository struct {
	client *redis.Client
	ctx    context.Context
}

func NewRedisRepository(redisHost, redisPort, redisPassword string) *RedisRepository {
	client := redis.NewClient(&redis.Options{
		Addr:     fmt.Sprintf("%s:%s", redisHost, redisPort),
		Password: redisPassword,
		DB:       0,
	})
	// Buat konteks untuk operasi Redis
	ctx := context.Background()
	return &RedisRepository{
		client: client,
		ctx:    ctx,
	}
}

func (r *RedisRepository) Set(key string, value string, expiration ...time.Duration) error {
	log.Println("Menyimpan data Redis key ", key)
	if len(expiration) > 0 {
		return r.client.Set(r.ctx, key, value, expiration[0]).Err()
	}
	return r.client.Set(r.ctx, key, value, 0).Err()
}
func (r *RedisRepository) Get(key string) (string, error) {
	log.Println("Memanggil data Redis key ", key)
	return r.client.Get(r.ctx, key).Result()
}

func (r *RedisRepository) Delete(key string) error {
	return r.client.Del(r.ctx, key).Err()
}
