package migration

import (
	"database/sql"
	"fmt"
	"log"
	"vandyahmad/avatara/config"

	_ "github.com/lib/pq"
	migrate "github.com/rubenv/sql-migrate"
)

// digunakan untuk runnign migrate otomatis
func RunningMigration(migrateStatus string) {
	if migrateStatus == "" {
		fmt.Println("Missing parameter, provide action!")
		return
	}

	username := config.EnvConfig.POSTGRES_USERNAME
	password := config.EnvConfig.POSTGRES_PASSWORD
	host := config.EnvConfig.POSTGRES_HOST
	port := config.EnvConfig.POSTGRES_PORT
	dbname := config.EnvConfig.POSTGRES_DATABASE

	psqlInfo := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Jakarta", host, username, password, dbname, port)

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	defer db.Close()

	switch migrateStatus {
	case "up":
		applyUpMigrations(db)
	case "down":
		applyDownMigrations(db)
	case "status":
		showMigrationStatus(db)
	default:
		fmt.Println("Invalid action!")
	}
}

func applyUpMigrations(db *sql.DB) {
	log.Println("running up migration")
	migrations := &migrate.FileMigrationSource{
		Dir: "migration",
	}
	m, err := migrate.Exec(db, "postgres", migrations, migrate.Up)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Applied %d migrations!\n", m)
}

func applyDownMigrations(db *sql.DB) {
	log.Println("running down migration")
	migrations := &migrate.FileMigrationSource{
		Dir: "migration",
	}
	m, err := migrate.ExecMax(db, "postgres", migrations, migrate.Down, 1)
	if err != nil {
		panic(err)
	}
	fmt.Printf("Applied %d migrations!\n", m)
}

func showMigrationStatus(db *sql.DB) {
	log.Println("running status migration")
	m, err := migrate.GetMigrationRecords(db, "postgres")
	if err != nil {
		panic(err)
	}
	for _, record := range m {
		fmt.Printf("%s - %s\n", record.Id, record.AppliedAt)
	}
}
