package main

import (
	"fmt"
	"net/http"
	"time"
	"vandyahmad/avatara/apps/migration"
	"vandyahmad/avatara/lib"

	"github.com/go-chi/chi/v5"
	"github.com/joho/godotenv"
	backendskeleton "vandyahmad/avatara"
	"vandyahmad/avatara/config"
	"vandyahmad/avatara/handler"
	"vandyahmad/avatara/lib/logger"
)

func init() {
	godotenv.Load()
	logger.Init()
}

func main() {

	db, err := config.NewPG()
	if err != nil {
		panic(err)
	}

	cfg := config.EnvConfig

	migration.RunningMigration(cfg.MIGRATION_EVENT)

	redis := lib.NewRedisRepository(cfg.REDIS_HOST, cfg.REDIS_PORT, cfg.REDIS_PASSWORD)
	openAI := lib.NewOpenAIRepostitory(cfg.OPENAPI_TOKEN)
	backendSkeleton := backendskeleton.NewBackendSkeleton(db, redis, openAI)
	handler := handler.NewHandler(&backendSkeleton)
	loc, _ := time.LoadLocation("Asia/Jakarta")
	time.Local = loc

	router := chi.NewRouter()

	router.Get("/healthz", handler.Healthz)

	router.Group(func(r chi.Router) {
		r.Use(handler.StandardMiddleware)
		r.Use(handler.PanicMiddlewares)

		r.Post("/chat", handler.CreateChat)
		r.Get("/chat", handler.ReadChat)

	})

	fmt.Println("Avatara Chat Boot Is Active By Vandy In Port: ", cfg.PORT)

	server := &http.Server{
		Addr:    fmt.Sprintf("0.0.0.0:%s", cfg.PORT),
		Handler: router,
	}

	err = server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}
