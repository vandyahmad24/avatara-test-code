-- +migrate Up
CREATE TABLE IF NOT EXISTS chats(
    id SERIAL NOT NULL PRIMARY KEY,
    message JSON,
    created_at TIMESTAMP NOT NULL,
    created_by INT,
    updated_at TIMESTAMP NOT NULL,
    updated_by INT
);

-- +migrate Down
DROP TABLE IF EXISTS chats;