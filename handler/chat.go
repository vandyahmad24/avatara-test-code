package handler

import (
	"encoding/json"
	"github.com/go-playground/validator/v10"
	"net/http"
	"vandyahmad/avatara/lib"
	"vandyahmad/avatara/request"
)

func (handler *Handler) CreateChat(rw http.ResponseWriter, r *http.Request) {
	var payload request.CreateChat

	err := json.NewDecoder(r.Body).Decode(&payload)
	if err != nil {
		WriteError(rw, lib.ErrorInvalidParameter)
		return
	}

	err = validator.New().Struct(payload)
	if err != nil {
		WriteError(rw, lib.ErrorInvalidParameter)
		return
	}

	res, err := handler.BackendSkeleton.Usecase.CreateChat(r.Context(), payload)
	if err != nil {
		WriteError(rw, err)
		return
	}

	WriteSuccess(rw, res, "success", ResponseMeta{HTTPStatus: http.StatusOK})
}

func (handler *Handler) ReadChat(rw http.ResponseWriter, r *http.Request) {

	res, err := handler.BackendSkeleton.Usecase.ReadChat(r.Context())
	if err != nil {
		WriteError(rw, err)
		return
	}

	WriteSuccess(rw, res, "success", ResponseMeta{HTTPStatus: http.StatusOK})
}
