package handler

import (
	"context"
	"net/http"

	"github.com/felixge/httpsnoop"
	"github.com/google/uuid"
	"vandyahmad/avatara/lib"
	"vandyahmad/avatara/lib/logger"
)

func (handler *Handler) StandardMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

		ctx := context.WithValue(r.Context(), "X-Request-ID", uuid.NewString())
		m := httpsnoop.CaptureMetrics(handler.PanicMiddlewares(next), w, r.WithContext(ctx))

		logger.Info(ctx, "http api request", map[string]interface{}{
			"method":   r.Method,
			"path":     r.URL,
			"status":   m.Code,
			"duration": m.Duration.Milliseconds(),
		})
	})
}

func (handler *Handler) PanicMiddlewares(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if rec := recover(); rec != nil {
				logger.Error(r.Context(), "panic occured", map[string]interface{}{
					"error": rec,
				})
				WriteError(w, lib.ErrorInternalServer)
			}
		}()

		next.ServeHTTP(w, r)
	})
}
