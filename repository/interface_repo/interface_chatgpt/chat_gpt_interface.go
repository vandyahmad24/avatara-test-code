package interface_chatgpt

import (
	"context"
	"vandyahmad/avatara/model"
)

type ChatGPTRepository interface {
	SendNewMessage(ctx context.Context, message string) (string, error)
	SendOldMessage(ctx context.Context, message string, data []model.MessageData) (string, error)
}
