package interface_redis

import (
	"time"
)

type RedisRepository interface {
	Set(key string, value string, expiration ...time.Duration) error
	Get(key string) (string, error)
	Delete(key string) error
}
