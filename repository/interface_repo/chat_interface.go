package interface_repo

import (
	"context"
	"vandyahmad/avatara/model"
)

type ChatRepository interface {
	CreateChat(ctx context.Context, chat model.Chat) (model.Chat, error)
	GetChatCount(ctx context.Context) (int, error)
	UpdateChat(ctx context.Context, chat model.Chat) (model.Chat, error)
	GetLastChat(ctx context.Context) (model.Chat, error)
}
