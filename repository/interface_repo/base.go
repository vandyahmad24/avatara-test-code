package interface_repo

import (
	"context"
)

type BaseRepository interface {
	Transaction(ctx context.Context, fn func(context.Context) error) error
	ChatRepository
	//RedisRepository
	//ChatGPTRepository
}
