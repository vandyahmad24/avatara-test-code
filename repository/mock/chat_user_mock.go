package mock

import (
	"context"
	"github.com/stretchr/testify/mock"

	"vandyahmad/avatara/model"
)

func (m *MockRepository) CreateChat(ctx context.Context, chat model.Chat) (model.Chat, error) {
	arguments := m.Called(mock.AnythingOfType("model.Chat"))
	user := arguments.Get(0).(model.Chat)

	if arguments.Error(1) != nil {
		return model.Chat{}, arguments.Error(1)
	}

	return user, nil
}

func (m *MockRepository) GetChatCount(ctx context.Context) (int, error) {
	args := m.Called(ctx)

	if r1, ok := args.Get(0).(int); ok {
		return r1, args.Error(1)
	}

	return 0, args.Error(1)
}

func (m *MockRepository) UpdateChat(ctx context.Context, chat model.Chat) (model.Chat, error) {
	arguments := m.Called(chat)
	chat = arguments.Get(0).(model.Chat)

	if arguments.Error(1) != nil {
		return model.Chat{}, arguments.Error(1)
	}

	return chat, nil
}

func (m *MockRepository) GetLastChat(ctx context.Context) (model.Chat, error) {
	args := m.Called(ctx)

	if r1, ok := args.Get(0).(model.Chat); ok {
		return r1, args.Error(1)
	}

	return model.Chat{}, args.Error(1)
}
