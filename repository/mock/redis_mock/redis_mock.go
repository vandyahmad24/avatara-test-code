package redis_mock

import (
	"github.com/stretchr/testify/mock"
	"time"
)

type MockRedisRepository struct {
	mock.Mock
}

func (m *MockRedisRepository) Set(key string, value string, expiration ...time.Duration) error {
	args := m.Called(key, value, expiration)

	if errArg := args.Get(0); errArg != nil {
		return errArg.(error)
	}

	return nil
}
func (m *MockRedisRepository) Get(key string) (string, error) {
	args := m.Called(key)

	if errArg := args.Get(1); errArg != nil {
		return "", errArg.(error)
	}

	return args.String(0), nil
}

func (m *MockRedisRepository) Delete(key string) error {
	args := m.Called(key)

	if errArg := args.Get(0); errArg != nil {
		return errArg.(error)
	}

	return nil
}
