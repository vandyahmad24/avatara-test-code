package mock

import (
	"context"

	"github.com/stretchr/testify/mock"
)

type MockRepository struct {
	mock.Mock
}

func (m *MockRepository) Transaction(ctx context.Context, fn func(context.Context) error) error {
	arguments := m.Called(fn)
	if err := fn(ctx); err != nil {
		return err
	}
	return arguments.Error(0)
}
