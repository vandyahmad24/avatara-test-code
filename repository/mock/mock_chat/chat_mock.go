package mock_chat

import (
	"context"
	"vandyahmad/avatara/model"

	"github.com/stretchr/testify/mock"
)

type MockChatGPTRepository struct {
	mock.Mock
}

func (m *MockChatGPTRepository) SendNewMessage(ctx context.Context, message string) (string, error) {
	args := m.Called(message)

	if errArg := args.Get(1); errArg != nil {
		return "", errArg.(error)
	}

	return args.String(0), nil
}

func (m *MockChatGPTRepository) SendOldMessage(ctx context.Context, message string, data []model.MessageData) (string, error) {
	args := m.Called(message)

	// If the 'error' argument in the mock is set, return that error.
	if errArg := args.Get(0); errArg != nil {
		return "", errArg.(error)
	}

	return "", nil
}
