package repository

import (
	"context"
	"encoding/json"
	"vandyahmad/avatara/lib"
	"vandyahmad/avatara/lib/logger"
	"vandyahmad/avatara/model"
)

func (repo *Repository) CreateChat(ctx context.Context, chat model.Chat) (model.Chat, error) {
	tx, ok := ctx.Value("Trx").(*lib.Database)
	if !ok {
		tx = repo.db
	}
	logger.Info(ctx, "Info CreateChat", map[string]interface{}{
		"payload": chat,
		"tags":    []string{"postgres", "chat", "repo"},
	})

	jsonData, err := json.Marshal(chat.ChatData)
	if err != nil {
		return chat, err
	}

	chat.Message = jsonData

	err = tx.Create(&chat).Error
	if err != nil {
		logger.Error(ctx, "Error CreateChat", map[string]interface{}{
			"error": err,
			"tags":  []string{"postgres", "chat", "repo"},
		})

		return chat, err
	}

	return chat, nil
}

func (repo *Repository) GetChatCount(ctx context.Context) (int, error) {
	var count int64
	tx, ok := ctx.Value("Trx").(*lib.Database)
	if !ok {
		tx = repo.db
	}

	result := tx.Model(&model.Chat{}).Count(&count)
	if result.Error != nil {
		logger.Error(ctx, "Error GetChatCount", map[string]interface{}{
			"error": result.Error,
			"tags":  []string{"postgres", "chat", "repo"},
		})
		return 0, result.Error
	}

	return int(count), nil
}

func (repo *Repository) UpdateChat(ctx context.Context, chat model.Chat) (model.Chat, error) {
	tx, ok := ctx.Value("Trx").(*lib.Database)
	if !ok {
		tx = repo.db
	}
	logger.Info(ctx, "Info UpdateChat", map[string]interface{}{
		"payload": chat,
		"tags":    []string{"postgres", "chat", "repo"},
	})

	jsonData, err := json.Marshal(chat.ChatData)
	if err != nil {
		return chat, err
	}

	chat.Message = jsonData

	err = tx.Save(&chat).Error
	if err != nil {
		logger.Error(ctx, "Error UpdateChat", map[string]interface{}{
			"error": err,
			"tags":  []string{"postgres", "chat", "repo"},
		})

		return chat, err
	}

	return chat, nil
}

func (repo *Repository) GetLastChat(ctx context.Context) (model.Chat, error) {
	var chat model.Chat
	tx, ok := ctx.Value("Trx").(*lib.Database)
	if !ok {
		tx = repo.db
	}

	result := tx.Model(&chat).Last(&chat)
	if result.Error != nil {
		logger.Error(ctx, "Error GetChatCount", map[string]interface{}{
			"error": result.Error,
			"tags":  []string{"postgres", "chat", "repo"},
		})
		return chat, result.Error
	}

	return chat, nil
}
